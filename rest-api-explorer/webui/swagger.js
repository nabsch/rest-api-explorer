
function get_rest_path(prefix,tr,node)
{
    var tnd = tr.get_node(node);
    
    var txt = tr.get_text(node);
    if (txt[0]==='/') return [txt,[]];
    
    if (!tnd.original || !tnd.original.yang) return [prefix,[]];
    
    var rest = rest_text(tr,node);
    
    var path = rest[0];
    var params = rest[1];
    
    var leafentry = is_instance(tr.get_text(node));
       
    for (var nd=tr.get_parent(node); nd; nd=tr.get_parent(nd))
    {
        var txt = tr.get_text(nd);
        if (!txt) break;

        var tnd = tr.get_node(nd);       
        if (tnd.original && tnd.original.root2) break;       
        
        var kind = get_node_kind(nd,tr);
        if (kind === 'choice' || kind === 'case') continue;

        if (!leafentry && (!IsRestconf() || path[0]!='=')) path = "/"+path;
        leafentry = is_instance(txt);
          
        rest = rest_text(tr,nd);

        path = rest[0] + path;
                       
        for (var j=0;j<rest[1].length;j++) params.unshift(rest[1][j]);
    }

    return [prefix+"/"+path, params];
}

function qparam_none(name,desc)
{
    return {
        in: "query",
        name: name,
        type: "string",
        description: desc+"(*)",
        required: false,
        enum : ["true"]
        //allowEmptyValue : true
    };
}

function qparam(name,type,desc)
{
    return {
        in: "query",
        name: name,
        type: type,
        description: desc,
        required: false
    };
}

function query_params_get()
{
    var out = [];
    
    if (IsRestconf()) {
      out.push({
          in: "query",
          name: "content",
          description: "Select config and/or non-config data resources",
          type: "string",
          enum: ["config","nonconfig","all"]
      });

      out.push(qparam("depth","string","Request limited subtree depth in the reply content. 1...65535 or 'unbounded'"));
      out.push(qparam("fields","string","Request a subset of the target resource contents"));
      out.push(qparam("filter","string","Boolean notification filter for event stream resources"));
      out.push(qparam("start-time","string","Replay buffer start time for even stream resources"));
      out.push(qparam("stop-time","string","Replay buffer stop time for event stream resources"));
      out.push(qparam("trace-id","string","Provide a trace id to identify the transaction. We can use up to 64 character"));

      if (IsRestconfData()) {
        out.push(qparam_none("with-service-meta-data","Retrieve service metadata"));
      }


    } else {

      out.push(qparam_none("deep","Retrieve a resource with all subresources inline."));
      out.push(qparam_none("shallow","Retrieve a resource with no subresources inline."));

      out.push(qparam("limit","integer","Limited set of list entries to retrieve."));
      
      out.push(qparam("offset","integer","Specify a limited set of list entries to retrieve."));

      out.push(qparam("operations","boolean","To include/exclude operations (tailf:actions)"
              + " in the result. Default value is true,"));
      
      out.push(qparam("select","string","Select which nodes and subresources in a resource to retrieve, e.g., 'name;config(hostname)'"));

      out.push(qparam_none("verbose","Control display of the 'self' and 'path' attributes of the resource."));
    }

    out.push(qparam("unhide","string","Unhide hidden nodes. Comma-separated list of <groupname>[;<password>], e.g., 'extra,debug;secret' unhides normal group 'extra' and password-protected group 'debug' with password 'secret'."));

    out.push({
        in: "query",
        name: "with-defaults",
        type: "string",
        description: "Control display of default data. No value specified will be interpreted as 'report-all'."
            + " 'explicit' means a data node that has been explicitly set (even if value coincides with the default);"
            + " 'trim' will exclude nodes containing default value even if explicitly set to the default;\n"
            + " 'report-all-tagged': similar to 'report-all' except a data node considered by the server to contain "
            + "default value will include an attribute to indicate this condition",
        enum: ["report-all","explicit","trim","report-all-tagged"]
    });
               
    return out;
}

function query_params(cmd,showall)
{
    var out = [];
    
    if (cmd !=='DELETE')
    {
        var body = {
            in: "body",
            name: "body",
            description: "Payload spec."
        };
        
        if (!showall) body["schema"] = {"$ref": cmd==='PATCH' ? "#/definitions/ModelPatch" : "#/definitions/ModelDefault" };

        out.push(body);
    }
    
    if (cmd ==='POST' || IsRestconf() && cmd==='PUT')
    {
        var insert = {
            in: "query",
            name: "insert",
            description: "Specify where a resource should be inserted in a ordered-by user list."
                        +" This query parameter is used together with the resource query parameter.",
            type: "string",
            enum: ["after","before","first","last"]
        };
        
        out.push(insert);

        if (IsRestconf()) {
          out.push(qparam("point","string","Specify where a resource should be inserted in a ordered-by user list. "
                + " The format is the URI to an existing element in the list."
                + " This query parameter is used together with the insert query parameter."));
        } else {
          out.push(qparam("resource","string","Specify where a resource should be inserted in a ordered-by user list. "
                + " The format is the URI to an existing element in the list."
                + " This query parameter is used together with the insert query parameter."));
        }
    }    

    var dryrun = {
		in: "query",
		name: "dryrun",
		description: "No data is sent to the devices. Instead the effects that would have taken place is showed in the returned diff output.",
		type: "string",
		enum: ["xml","cli","native"]
    };
    
    out.push(dryrun);
    
    out.push(qparam_none("async-commit-queue","If some device is non-operational or has data waiting in the"
            + " commit queue, the data in this transaction will be placed in the commit queue"));
    
    out.push(qparam("sync-commit-queue","integer","An optional timeout value in form of a positive integer." 
            + " This value represents the number of seconds to wait for the transaction to be committed. "
            + " If the timeout expires, a commit queue Id in will be returned. "
            + " The flag will cause the operation to not return until the transaction has been sent to all devices,"
            + " or the specified timeout occurs."));
        
    out.push(qparam_none("no-out-of-sync-check","Continue with the transaction even if NCS detects that a"
            + " device's configuration is out of sync."));
    
    out.push(qparam_none("no-overwrite","NCS will check that the data that should be modified has not"
            + " changed on the device compared to NCS's view of the data"));
    
    out.push(qparam_none("no-networking","Do not send any data to the devices. This is a way to manipulate"
            + " CDB in NCS without generating any southbound traffic"));

    if (IsRestconf()) {
    	out.push(qparam("trace-id","string","Provide a trace id to identify the transaction. We can use up to 64 character"));
    }
    
    out.push(qparam_none("rollback-id","Return the rollback id in the response if a rollback file was"
            + " created during this operation. This requires rollbacks to be"
            + " enabled in the NSO to take effect."));
   
    out.push(qparam("rollback-comment","string","Used to specify a comment to be attached to the Rollback File"
            + " that will be created as a result of the POST operation."
            + "  This assume that Rollback File handling is enabled."));

    out.push(qparam("rollback-label","string","Used to specify a label to be attached to the Rollback File"
            + " that will be created as a result of the POST operation."
            + "  This assume that Rollback File handling is enabled."));
			
    out.push(qparam_none("dry-run-reverse","Used together with the dry-run=native parameter to display the"
            + " device commands for getting back to the current running state"
            + " in the network if the commit is successfully executed. Beware"
            + " that if any changes are done later on the same data the reverse"
            + " device commands returned are invalid."));

    out.push(qparam_none("no-revision-drop","NSO will not run its data model revision algorithm, which"
            + " requires all participating managed devices to have all parts"
            + " of the data models for all data contained in this transaction."
            + " Thus, this flag forces NSO to never silently drop any data set"
            + " operations towards a device."));

    out.push(qparam_none("no-deploy","Commit without invoking the service create method, i.e, write" 
            + " the service instance data without activating the service(s). The"
            + " service(s) can later be re-deployed to write the changes of the"
            + " service(s) to the network."));

    out.push(qparam_none("use-lsa","Force handling of the LSA nodes as such. This flag tells NSO" 
            + " to propagate applicable commit flags and actions to the LSA"
            + " nodes without applying them on the upper NSO node itself."
            + " The commit flags affected are dry-run, no-networking,"
            + " no-out-of-sync-check, no-overwrite and norevision-drop."));

    out.push(qparam_none("no-lsa","Do not handle any of the LSA nodes as such. These nodes will"
            + " be handled as any other device"));

    return out;
}

function respCode(cmd)
{
    var phschema = {type: "string"};   
    
    var allCode = {
        200: {description: "OK",schema: phschema, details: "The request was successfully completed, and a response body is returned containing a representation of the resource."},
        201: {description: "Created",schema: phschema, details: "A resource was created, and the new resource URL is returned in the Location header"},
        204: {description: "No content",schema: phschema,details: "The request was successfully completed, but no response body is returned."},
        400: {description: "Bad Request", details: "The request could not be processed because it contains missing or invalid information (such as validation error on an input field, a missing required value, and so on)."},
        401: {description: "Unauthorized", details: "The request requires user authentication. The response includes a WWW-Authenticate header field for basic authentication."},
        403: {description: "Forbidden", details: "Access to the resource was denied by the server, due to authorization rules."},
        404: {description: "Not Found", details: "The requested resource does not exist."},
        405: {description: "Method Not Allowed", details: "The HTTP method specified in the request (DELETE, GET, HEAD, PATCH, POST, PUT) is not supported for this resource."},
        406: {description: "Not Acceptable", details: "The resource identified by this request is not capable of generating the requested representation, specified in the Accept header or in the format query parameter."},
        409: {description: "Conflict", details: "This code is used if a request tries to create a resource that already exists."},
        415: {description: "Unsupported Media Type", details: "The format of the request is not supported."},
        500: {description: "Internal Error", details: "The server encountered an unexpected condition which prevented it from fulfilling the request."},
        501: {description: "Not Implemented", details: "The server does not (currently) support the functionality required to fulfill the request."},
        503: {description: "Unavailable", details: "The server is currently unable to handle the request due to the resource being used by someone else, or the server is temporarily overloaded."}
    };
        
    if (cmd === 'DELETE') delete allCode["200"];
        
    if (cmd === 'DELETE' || cmd === 'GET' || cmd === 'HEAD')
    {
        delete allCode["201"];
    }
        
    return allCode;
}

function swaggerSpec2(prefix, tr, node, showall)
{
    if (!node) return null;
    
    var yang = node.original ? node.original.yang : null;
    var kind = yang ? yang.kind : "";

    var spec = {
        swagger: "2.0",
        schemes: ["http"],
        info: {
            "version": "0.8.0",
            "title": "NSO REST API Explorer"
        },
        basePath: ""
    };
    
    var rest = get_rest_path(prefix, tr, node);
    //alert(JSON.stringify(rest));

    var url = rest[0];
    var pparams = rest[1];   
   
    var fmt = IsRestconf() ? 'yang-data' : 'vnd.yang.data';
    var fmtGet = "";
        
    if (kind === 'list' || kind === 'leaf-list') { 
        fmtGet = 'vnd.yang.collection';

    } else if (kind === 'action') {
        fmt = IsRestconf() ? 'yang-data' : 'vnd.yang.operation';
        
    } else if (   url==='/api/rollbacks' 
               || url==='/api/rollbacks/{filename}' 
               || url==='/api/operations'
               || url==='/api') 
    {
        fmt = 'vnd.yang.api';
    } 
    else if (     url==='/api/running'
               || url==='/api/config'
               || url==='/api/operational') 
    {
        fmt = 'vnd.yang.datastore';
    }            

    if (fmtGet === "") fmtGet = fmt;
    
    var mimeGet = ["application/"+fmtGet+"+json","application/"+fmtGet+"+xml"];
    
    var mime = ["application/"+fmt+"+json","application/"+fmt+"+xml"];
    var mime2 = ["application/"+fmt+"+json","application/"+fmt+"+xml"];

    
    if (url==='/api/rollbacks/{filename}')
    {
        pparams = [{
            in: "path",
            name: "filename",
            description: "The file name returned by GET /api/rollbacks",
            type: "integer",
            required: true
        }];
    }
    else if (url.indexOf("{operation}") >=0 )
    {
        pparams = [{
            in: "path",
            name: "operation",
            description: "The operation returned by GET "+url,
            type: "string",
            required: true
        },{
            in: "body",
            name: "body",
            description: "Payload for the operation."
        } ];
    }
 
    var tagname = url;//yang.name
 
    var tag = {name: tagname};
    
    if (yang && yang.info)
    {
        if (showall) tag["description"] = yang.info.string;
        else spec["info"] = {"description": yang.info.string};
    }   
       
    var note = "\n\nIf parameter description is marked(*), it should be sent without '=value', e.g, '?param'."
            + " '?param=true' is used here to get arround a swagger limitation, the value is ignored by the server.";

    var get = {
        tags: [tagname],
        summary: "Retrieve data and meta-data.",
        description: "Supported for all resource types, except operation resources."+note,
        produces: mimeGet,
        consumes: mimeGet,
        parameters: pparams.concat(query_params_get()),
        responses: respCode('GET')
    };

    var post = {
        tags: [tagname],
        summary: kind === 'action' ? "Invoke an operation resource." : "Create a data resource.",
        description: "Creates resources which may not exist beforehand."
                + " The URL should point to the parent of the resource to create"
                + " (So no KEYS in the URL unless when creating a resource under an existing list instance)."
                + " When creating a list instance, POST to the parent resource, and payload must contain the key(s)."
                + " POST returns the location of the created resource (so only one resource can be created for each request).\n\n"
                + " If the target resource type is an operation resource, "
                + "then the POST method is treated as a request to invoke that operation." 
                + "The message body (if any) is processed as the operation input parameters."+note,
        produces: mime,
        consumes: mime2,
        parameters: pparams.concat(query_params('POST',showall)),
        responses: respCode('POST')
    };
    
    var put = {
        tags: [tagname],
        summary: "Create or replace the target resource.",
        description: "The request must contain a request URL that contains a target resource that identifies the data resource to create or replace."
                + " The URL should point to the resource to create/replace. When creating/replacing a list instance, the key(s) must be in the URL,"
                + " and the payload still needs to contain the keys.\n\n"
                + " Keep in mind that PUT will replace everything with the provided payload. So if any subnode is not present in the payload, it will be deleted."
                + note,
        produces: mime,
        consumes: mime,
        parameters: pparams.concat(query_params('PUT',showall)),
        responses: respCode('PUT')
    };

    var patch = {
        tags: [tagname],
        summary: "Updates an existing resource.",
        description: " PATCH is used to update an existing resource. This is only allowed on existing resources,"
                + " the payload should only contain the data that need to be modified."
                + " PATCH should not be used to create new resources directly, since it must operate on existing resources."
                + "\n\nIn contrast to PUT, PATCH only merges the provided payload with the existing configuration, which"
                + " makes it possible to create child resources within the target resource. "
                + " For example, list entries within a non-presence container can be created"
                + " using PATCH on the parent container; List entries within an existing list instance can be also "
                + " created using PATCH on the parent list instance.\n\n"
                + " Non-presence containers always exist, so they are always existing resources if parent exists. "
                + " A list instance is also a resource, but the list node itself (without keys) is not a resource."
                + " So When updating a list instance, the key(s) must be in the URL (do not include keys in the body)."
                + note,
        produces: mime,
        consumes: mime,
        parameters: pparams.concat(query_params('PATCH',showall)),
        responses: respCode('PATCH')
    }; 
     
    var delet = {
        tags: [tagname],
        summary: "Delete the target resource.",
        description: "Payload for the DELETE request is always ignored. "
                + "So if the URL points to the list, then all instances of the list will be deleted even if payload contains only one instance. "
                + "To delete a single instance, include the key(s) in the URL." + note,        
        produces: mimeGet,
        consumes: mimeGet,
        parameters: pparams.concat(query_params('DELETE',showall)),
        responses: respCode('DELETE')
    };
    
    var access = {get: get};
    
    if (kind==='action')
    {
         access = {post: post};
    }
    else if (url.indexOf("_rollback") >=0)
    {
        post.parameters = [{in: "body", name: "body", description: "File number.", 
                            schema: {"type": "object","properties": {"file": {type: "integer"}}, "xml" : {"name": "RemoveThisTagXML"} } 
                          }];
        access = {post: post};
    }  
    else if (url.indexOf("{operation}") >=0)
    {
        post.parameters = pparams;
        access = {post: post};
    }
    else if (yang && yang.access)
    {
        if (yang.access.create || yang.access.update || yang.access.execute) 
        {
            access["put"] = put;
            access["post"] = post;
            access["patch"] = patch;
        }

        if (yang.access.delete || kind === 'key') 
        {
            access["delete"] = delet;
        }
    }
    else if (kind=='module')
    {
        access["put"] = put;
        access["post"] = post;
        access["patch"] = patch;
    }

    var options = {
        tags: [tagname],
        summary: "Discover methods supported by the server for a specific resource.",
        description: "The supported methods are listed in the ALLOW header.",
        responses: {"200": {"description": "OK"}}
    };
    
    var head = {
        tags: [tagname],
        summary: "Retrieve just the headers without the response body.",
        description: "The access control behavior is enforced as if the method was GET instead of HEAD."
            + " The server will respond the same as if the method was GET instead of HEAD, except that no response body is included",
        responses: respCode('HEAD'),
        parameters: (IsRestconf() ? pparams.concat(query_params_get()) : null)
    };
    
    access["options"] = options;
    access["head"] = head;
   
    var path = {};
    path[url] = access;
        
    return [spec,tag,path];    
}

function swaggerSpec1(tr, node, showall)
{
    if (IsRestconf()) {
        var x = swaggerSpec2("/restconf/data", tr, node, showall);
        return [x[0], [x[1]], x[2]];
    } 

    var a = swaggerSpec2("/api/running", tr, node, showall);
    var b = swaggerSpec2("/api/config", tr, node, showall);
    var c = swaggerSpec2("/api/operational", tr, node, showall);
    
    //var d = swaggerSpec2("/api/operations", tr, node, showall);
    //var e = swaggerSpec2("/api/rollbacks", tr, node, showall);
    
    return [ 
        a[0], 
        [a[1],b[1],c[1]], 
        $.extend(a[2],b[2],c[2])
    ];
}

function swaggerSpec(tr, node)
{
    if (!node.original || !node.original.yang)
        return null;

    var s = swaggerSpec1(tr,node,false);
    
    return $.extend(s[0], {tags: s[1]}, {paths: s[2]});
}

function appendArray(a,b)
{
    for (var i=0;i<b.length;i++) a.push(b[i]);
    
    return a;
}

var _tags=[];
var _paths={};
function swaggerExport2(tr,node)
{
    var children = tr.get_children_dom(node);    
    for (var i=0;i<children.length;i++)
    {
        var ch = tr.get_node(children[i]);
        
        var top = swaggerSpec1(tr,ch,true);
        
        _tags = appendArray(_tags,top[1]);
        _paths = $.extend(_paths,top[2]);
        
        swaggerExport2(tr,ch);
    }
}
    
function swaggerExport()
{    
    var tr = $('#myTree').jstree(true);    
    var sel = tr.get_selected(true);
    if (sel.length<1) {alert("Select a node from the tree first");return;}
    
    var node = sel[0];    
    
    var msg = 'This will export swagger spec for all visible nodes of the subtree ' +
        'rooted at [' + tr.get_text(node) + ']. ' +
        'To select a different subtree, just click its root node, then Export.' +
        '\n\nContinue?';

    if (!confirm(msg)) return;
    
    loadingSwagger(true);
    setTimeout(()=>{
        exportSpec(tr,node);
        loadingSwagger(false);
    },500);
}

function exportSpec(tr,node) {

    var top = swaggerSpec1(tr,node,true);
    
    var spec = top[0];
    _tags = top[1];
    _paths = top[2];

    swaggerExport2(tr,node);

    var specs = $.extend(spec, {tags: _tags}, {paths: _paths});
    var txt = JSON.stringify(specs,null,2);
    
    //spec['definitions'] = get_definitions(tr,node);
    //takes too long: swaggerNode1(specs,"none");
    
    var myout = myOut('yangPane');
    myout("<pre>"+txt+"</pre>");

    if (confirm('Save the spec as file?')) {
        saveAsFile(txt, 'spec.json', 'text/plain');
    }
}

function saveAsFile(content, fileName, contentType) {
    var a = document.createElement("a");
    var file = new Blob([content], {type: contentType});
    a.href = URL.createObjectURL(file);
    a.download = fileName;
    a.click();
}

function swaggerNode(tr, node, prefix)
{    
    var spec = {};
    
    if (prefix === "/api") {
        spec = swaggerSpec(tr, node);
    } else {
        var s = swaggerSpec2(prefix, tr, node, false);
    
        if (s) spec = $.extend(s[0], {tags: s[1]}, {paths: s[2]});
    }
    
    spec['definitions'] = get_definitions(tr,node);
    return swaggerNode1(spec,"list");
}

function swaggerNode1(spec,docExp)
{
    if (!spec) return;    
    
    var url = window.location.search.match(/url=([^&]+)/);
    if (url && url.length > 1) {
        url = decodeURIComponent(url[1]);
    } else {
        url = "127.0.0.1:8080";
    }
    
    /****
    parent.yangpane.document.body.innerHTML = '';  
    var myout = document.write.bind(parent.yangpane.document);
    myout("<pre>"+JSON.stringify(spec,null,2)+"</pre>");
    /****/
   
    var swaggerUi = new SwaggerUi({
        url : url,
        spec: spec,
        validatorUrl: null,
        dom_id: "swagger-ui-container",
        //useJQuery: true,
        supportedSubmitMethods: ['get','post','put','patch','delete','options','head'],
        docExpansion: docExp,
        showRequestHeaders: false,
        onComplete: function(swaggerApi, swaggerUi)
        {
            $('pre code').each(function (i, e) {
                hljs.highlightBlock(e)
            });

            addAuthorization(swaggerUi);
        },
        onFailure: function(data)
        {
            swaggerLog("Unable to Load SwaggerUI");
        }
    });

    var nso = ''; //default is NSO in address bar

    swaggerUi.setOption('myhost',nso);

    //swaggerUi.setOption('swaggerRequestHeaders','application/vnd.yang.data+json');
        
    swaggerUi.load();
}

function addAuthorization(swaggerUi)
{
    if (!swaggerUi) return;

    var login = getUsernamePasswd();

    var username = login.username;
    var password = login.password;

    if (username && username.trim() != "" && password && password.trim() != "")
    {
        var basicAuth = new SwaggerClient.PasswordAuthorization('basic', username, password);
        swaggerUi.api.clientAuthorizations.add("basicAuth", basicAuth);
        //swaggerLog("authorization added: username = " + username + ", password = " + password);
    }
}

function swaggerLog()
{
    if ('console' in window) {
        console.log.apply(console, arguments);
    }
}
