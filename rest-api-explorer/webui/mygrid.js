// function is used for dragging and moving
function dragElement(element, params) {
  var md; // remember mouse down info
  const pane1 = document.getElementById(params.pane1);
  const pane2 = document.getElementById(params.pane2);

  element.onmousedown = onMouseDown;

  function onMouseDown(e) {
    //console.log("mouse down: " + e.clientX);
    md = {
      e,
      offsetLeft: element.offsetLeft,
      offsetTop: element.offsetTop,

      pane1W: pane1.offsetWidth,
      pane2W: pane2.offsetWidth,
      pane1H: pane1.offsetHeight,
      pane2H: pane2.offsetHeight
    };
    document.onmousemove = onMouseMove;
    document.onmouseup = () => {
      //console.log("mouse up");
      document.onmousemove = document.onmouseup = null;
    }
  }

  function onMouseMove(e) {
    //console.log("mouse move: " + e.clientX);
    var delta = {
      x: e.clientX - md.e.x,
      y: e.clientY - md.e.y
    };

    if (params.dir === "H") // Horizontal
    {
      // prevent negative-sized elements
      delta.x = Math.min(Math.max(delta.x, -md.pane1W), md.pane2W);

      element.style.left = md.offsetLeft + delta.x + "px";
      pane1.style.width = (md.pane1W + delta.x) + "px";
      //pane2.style.width = (md.pane2W - delta.x) + "px";
    }
    else if (params.dir === "V") // Vertical
    {
      // prevent negative-sized elements
      delta.y = Math.min(Math.max(delta.y, -md.pane1H), md.pane2H);

      element.style.top = md.offsetTop + delta.y + "px";
      pane1.style.height = (md.pane1H + delta.y) + "px";
      //pane2.style.height = (md.pane2H - delta.y) + "px";
    }
  }
}
